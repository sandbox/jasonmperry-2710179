<?php /**
 * @file
 * Contains \Drupal\onelogin_saml\Controller\DefaultController.
 */

namespace Drupal\onelogin_saml\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Controller\ControllerBase;
use Onelogin_Saml2_Auth;

/**
 * Default controller for the onelogin_saml module.
 */
class DefaultController extends ControllerBase {

	public function getSettings(){
		require( dirname(__FILE__) . '/../../settings.php' );
		return $settings;
	}

	public function showSSO() {
    $this->onelogin_saml_sso();
  }

	public function showACS() {
    $this->onelogin_saml_acs();
  }

	public function showSLS() {
    $this->onelogin_saml_sls();
  }

	public function showMetadata() {
    $this->onelogin_saml_metadata();
  }

	private function onelogin_saml_sso() {
	  // If a user initiates a login while they are already logged in, simply send them to their profile.
	  if (\Drupal::currentUser()->isAuthenticated() && !\Drupal::currentUser()->isAnonymous()) {
			$response = new RedirectResponse('/user/' . \Drupal::currentUser()->id());
  		$response->send();
	  }
	  $auth = $this->initialize_saml();
	  if (isset($_GET['destination'])) {
	    $target = $_GET['destination'];
	  } else if (isset($_GET['returnTo'])) {
	    $target = $_GET['returnTo'];
	  }
	  if (isset($target) && strpos($target, 'onelogin_saml/sso') === FALSE) {
	    $auth->login($target);
	  } else {
	    $auth->login();
	  }
	  exit();
	}

	private function onelogin_saml_slo() {
	  global $cookie_domain, $user;

	  session_destroy();
	  $auth = $this->initialize_saml();
	  $auth->logout('/');
	  exit();
	}

	private function onelogin_saml_acs() {

	  // If a user initiates a login while they are already logged in, simply send them to their profile.
	  if (\Drupal::currentUser()->isAuthenticated() && !\Drupal::currentUser()->isAnonymous()) {
			$response = new RedirectResponse('/user/' . \Drupal::currentUser()->id());
  		$response->send();
	  }
	  else if (isset($_POST['SAMLResponse']) && !empty($_POST['SAMLResponse'])){
	    $auth = $this->initialize_saml();

	    $auth->processResponse();

	    $errors = $auth->getErrors();
	    if (!empty($errors)) {
	    	\Drupal::logger('onelogin_saml')->error("There was at least one error processing the SAML Response".implode("<br>", $errors));
	      drupal_set_message("There was at least one error processing the SAML Response".implode("<br>", $errors), 'error', FALSE);
	      $this->redirect('');
	    }
	    $this->onelogin_saml_auth($auth);
	  }
	  else {
	  	\Drupal::logger('onelogin_saml')->error("No SAML Response found.");
	    drupal_set_message("No SAML Response found.", 'error', FALSE);
	    $this->redirect('');
	  }

		$response = new RedirectResponse('/user/' . \Drupal::currentUser()->id());
  	$response->send();
  	exit();
	}

	private function onelogin_saml_sls() {
	  $auth = $this->initialize_saml();
	  $auth->processSLO();
	  $errors = $auth->getErrors();
	  if (empty($errors)) {
	      session_destroy();
	  }
	  else {
	    $reason = $auth->getLastErrorReason();
	    drupal_set_message("SLS endpoint found an error.".$reason, 'error', FALSE);
	  }
	  
	  if (isset($_GET ['destination']) && strpos($_GET ['destination'], 'user/logout') !== FALSE) {
	     unset($_GET ['destination']);
	  }
	  
	  $this->redirect('');
	}

	private function onelogin_saml_metadata() {
	  $auth = $this->initialize_saml();
	  $auth = new Onelogin_Saml2_Auth($this->getSettings());
	  $settings = $auth->getSettings();
	  $metadata = $settings->getSPMetadata();
	  header('Content-Type: text/xml');
	  echo $metadata;
	  exit();
	}

	private function onelogin_saml_auth($auth) {
	  $username = '';
	  $email = '';
	  $autocreate = \Drupal::config('onelogin_saml.settings')->get('saml_options_autocreate');

	  // Get the NameId.
	  $nameId = $auth->getNameId();

	  if (empty($nameId)) {
	  	\Drupal::logger('onelogin_saml')->error("A NameId could not be found. Please supply a NameId in your SAML Response.");
	  	drupal_set_message("A NameId could not be found. Please supply a NameId in your SAML Response.", 'error', FALSE);
	  	$this->redirect('');	
	  }

	  // Get SAML attributes
	  $attrs = $auth->getAttributes();

	  $usernameFromEmail = \Drupal::config('onelogin_saml.settings')->get('saml_options_username_from_email');

	  if (!empty($attrs)) {
	    $usernameMapping = \Drupal::config('onelogin_saml.settings')->get('saml_attr_mapping_username');
	    $mailMapping =  \Drupal::config('onelogin_saml.settings')->get('saml_attr_mapping_email');

	    // Try to get $email and $username from attributes of the SAML Response
	    if (!empty($usernameMapping) && isset($attrs[$usernameMapping]) && !empty($attrs[$usernameMapping][0])){
	      $username = $attrs[$usernameMapping][0];
	    }
	    if (!empty($mailMapping) && isset($attrs[$mailMapping])  && !empty($attrs[$mailMapping][0])){
	      $email = $attrs[$mailMapping][0];
	    }
	  }

	  // If there are attrs but the mail is in NameID try to obtain it
	  if (empty($email) && strpos($nameId, '@')) {
	    $email = $nameId;
	  }

	  if (empty($username) && $usernameFromEmail) {
	    $username = str_replace('@', '.', $email);
	  }

	  $matcher = \Drupal::config('onelogin_saml.settings')->get('saml_options_account_matcher');
	  if ($matcher == 0) {
	    if (empty($username)) {
	    	\Drupal::logger('onelogin_saml')->error("Username value not found on the SAML Response. Username was selected as the account matcher field. Review at the settings the username mapping and be sure that the IdP provides this value");
	      drupal_set_message("Username value not found on the SAML Response. Username was selected as the account matcher field. Review at the settings the username mapping and be sure that the IdP provides this value", 'error', FALSE);
	      $this->redirect();
	    }
	    // Query for active users given an usermail.
	    $query = \Drupal::entityQuery('user');
	    $query->condition('status', 1)
	          ->condition('name', $username);
	  }
	  else {
	    if (empty($email)) {
	    	\Drupal::logger('onelogin_saml')->error("Email value not found on the SAML Response. Email was selected as the account matcher field. Review at the settings the username mapping and be sure that the IdP provides this value");
	      drupal_set_message("Email value not found on the SAML Response. Email was selected as the account matcher field. Review at the settings the username mapping and be sure that the IdP provides this value", 'error', FALSE);
	      $this->redirect();
	    }
	    // Query for active users given an e-mail address.
	    $query = \Drupal::entityQuery('user');
	    $query->condition('status', 1)
	          ->condition('mail', $email);
	  }

	  $syncroles = \Drupal::config('onelogin_saml.settings')->get('saml_options_syncroles');

	  $roles = array();
	  if ($syncroles) {

	    // saml_attr_mapping_role
	    $roleMapping = \Drupal::config('onelogin_saml.settings')->get('saml_attr_mapping_role');

	    if (!empty($roleMapping) && isset($attrs[$roleMapping]) && !empty($attrs[$roleMapping])) {

	    	//get an array of all the assigned roles
	      $assignedRoles = explode(';',$attrs[$roleMapping][0]);

    		// list all roles in drupal
    		$allRoles = \Drupal\user\Entity\Role::loadMultiple();

	      //iterate through each possible roll option
    		foreach ($allRoles as $key => $value) {

 			     //if this is authenticated or anon skip it
      		if( $key == 'anonymous' || $key == 'authenticated' ){
        		continue;
      		}

      		//get the matching onelogin roles for this role
      		$mappingChoices = explode(',',\Drupal::config('onelogin_saml.settings')->get('saml_role_mapping_roles_'.$key));

      		//iterate through all the assigned roles
      		foreach ($assignedRoles as $choice) {

      			//adds the role into the listing of options
      			if( in_array($choice,$mappingChoices)){
      				array_push($roles, $key);
      			}
      		}

      	}
      }
	  }

	  // If a user exists, attempt to authenticate.
	  $result = $query->execute();

	  if ($result && $user = user_load(key($result), TRUE)) {
	    //$GLOBALS['user'] = $user;  //global $user depricated 
	    //$form_state['uid'] = $user->id();

	  	// remove roles from the current user
	  	if ($syncroles) {

	      //iterate through each possible role option
    		foreach ($allRoles as $key => $value) {

 			     //if this is authenticated or anon skip it
      		if( $key == 'anonymous' || $key == 'authenticated' ){
        		continue;
      		}

      		//remove roles no longer needed
      		if( !in_array($key, $roles) ){
  	    		$user->removeRole($key);
  	    	}
  	    }
	  	}

	    if (!empty($roles)) {
	      try {
	      	foreach ($roles as $key) {
	      		$user->addRole($key);	//adds a role for each role id
	      	}
        
	       $user->save();
	      }
	      catch (Exception $e) {
	        return FALSE;
	      }
	    }
	    user_login_finalize($user);
		  user_cookie_save(array('drupal_saml_login'=>'1'));

	  } else if ($autocreate) {

	    // If auto-privisioning is enabled but there are no required attributes, we need to stop.
	    if (empty($email) || empty($username)) {
	    	\Drupal::logger('onelogin_saml')->error("Auto-provisioning accounts requires a username and email address. Please supply both in your SAML response.");
	      drupal_set_message("Auto-provisioning accounts requires a username and email address. Please supply both in your SAML response.", 'error', FALSE);
	      $this->redirect();
	    }

	    $user = \Drupal\user\Entity\User::create();
	    $user->enforceIsNew();
	    $user->setUsername($username);
	    $user->setEmail($email);
	    $user->setPassword(user_password(16));
			$user->set("init", $email);
	    $user->activate();

	    if (!empty($roles)) {
	      foreach ($roles as $key) {
	      	$user->addRole($key);	//adds a role for each role id
	      }
	    }

	    try {
	      $user->save();
	      user_login_finalize($user);
		    user_cookie_save(array('drupal_saml_login'=>'1'));
	    }
	    catch (Exception $e) {
	      return FALSE;
	    }
	  }
	  else {
	    drupal_set_message("User '".($matcher == 'username'? $username : $email). "' not found.", 'error', FALSE);
	    $this->redirect();
	  }
	}

	private function initialize_saml() {

	  require_once( dirname(__FILE__) . '/../../_toolkit_loader.php' );

	  try {
	    $auth = new Onelogin_Saml2_Auth($this->getSettings());
	  } catch (Exception $e) {
	    drupal_set_message("The Onelogin SSO/SAML plugin is not correctly configured:".'<br>'.$e->getMessage(), 'error', FALSE);
	    $this->redirect();
	  }

	  return $auth;
	}

}

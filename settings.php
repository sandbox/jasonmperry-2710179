<?php
global $base_url;
$config = \Drupal::config('onelogin_saml.settings');
$settings = array (

  'strict' => $config->get('saml_advanced_settings_strict_mode'),
  'debug' => $config->get('saml_advanced_settings_debug'),

  'sp' => array (
    'entityId' => $config->get('saml_advanced_settings_sp_entity_id'),
    'assertionConsumerService' => array (
      'url' => $base_url . \Drupal\Core\Url::fromRoute('onelogin_saml.acs')->toString(),
    ),
    'singleLogoutService' => array (
      'url' => $base_url . \Drupal\Core\Url::fromRoute('onelogin_saml.sls')->toString(),
    ),
    'NameIDFormat' => $config->get('saml_advanced_settings_nameid_format'),
    'x509cert' => $config->get('saml_advanced_settings_sp_x509cert'),
    'privateKey' => $config->get('saml_advanced_settings_sp_privatekey'),
  ),

  'idp' => array (
    'entityId' => $config->get('saml_idp_entityid'),
    'singleSignOnService' => array (
      'url' => $config->get('saml_idp_sso'),
    ),
    'singleLogoutService' => array (
      'url' => $config->get('saml_idp_slo'),
    ),
    'x509cert' => $config->get('saml_idp_x509cert'),
  ),

  'security' => array (
    'signMetadata' => false,
    'nameIdEncrypted' => $config->get('saml_advanced_settings_nameid_encrypted'),
    'authnRequestsSigned' => $config->get('saml_advanced_settings_authn_request_signed'),
    'logoutRequestSigned' => $config->get('saml_advanced_settings_logout_request_signed'),
    'logoutResponseSigned' => $config->get('saml_advanced_settings_logout_response_signed'),
    'wantMessagesSigned' => $config->get('advanced_settings_want_message_signed'),
    'wantAssertionsSigned' => $config->get('saml_advanced_settings_want_assertion_signed'),
    'wantAssertionsEncrypted' => $config->get('saml_advanced_settings_want_assertion_encrypted'),
  )
);
